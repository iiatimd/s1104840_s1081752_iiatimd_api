<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rit extends Model
{
    protected $fillable = [
        'id',
        'user_id',
        'user_name',
        'rit_start',
        'rit_eind',
        'km_start',
        'km_eind',
        'kenteken',
        'beschrijving'
    ];


    protected $table = 'ritten';
}
