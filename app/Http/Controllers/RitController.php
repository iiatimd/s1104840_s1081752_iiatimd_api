<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Rit;

class RitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(
            Rit::all()
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rit = new Rit();

            $rit->id = $request['id'];
            $rit->user_id = $request['user_id'];
            $rit->user_name = $request['user_name'];
            $rit->rit_start = $request['rit_start'];
            $rit->rit_eind = $request['rit_eind'];
            $rit->km_start = $request['km_start'];
            $rit->km_eind = $request['km_eind'];
            $rit->kenteken = $request['kenteken'];
            $rit->beschrijving = $request['beschrijving'];

            $rit->save();

        // $rit = Ritten::create([
        //     'id' => $request->get('id'),
        //     'rit_naam' => $request->get('rit_naam'),
        //     'user_id' => $request->get('user_id'),
        //     'user_name' => $request->get('user_name'),
        //     'km_begin' => $request->get('km_begin'),
        //     'km_eind' => $request->get('km_eind'),
        //     'rit_start' => $request->get('rit_start'),
        //     'rit_einde' => $request->get('rit_einde'),
        //     'kenteken_auto' => $request->get('kenteken_auto'),
        // ]);
        return response()->json($rit);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($user_id)
    {
        return response()->json([
            'ritten'=> ritten::findOrFail($user_id)
        ]);


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
