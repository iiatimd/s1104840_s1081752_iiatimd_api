<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// * Place unprotected routes here
Route::get('/', 'UserController@noAuth')->name('login');
Route::post('login', 'UserController@login');
Route::post('register', 'UserController@register');

Route::resource('rit', 'RitController');

// * Place protected routes within this group
Route::group(['middleware' => 'auth:api'], function () {

    Route::post('logout', 'UserController@logout');
    Route::post('refresh', 'UserController@refresh');
    Route::get('user', 'UserController@getAuthenticatedUser');

});
