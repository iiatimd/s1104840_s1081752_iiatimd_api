<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRittenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ritten', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->String('user_name');
            $table->String('rit_start');
            $table->String('rit_eind');
            $table->integer('km_start');
            $table->integer('km_eind');
            $table->String('kenteken');
            $table->String('beschrijving')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ritten');
    }
}
