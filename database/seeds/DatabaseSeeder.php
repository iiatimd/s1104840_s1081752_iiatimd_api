<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ritten')->insert([
            'id' => '1',
            'user_id' => 2,
            'user_name' => 'Roy',
            'km_start' => 123,
            'km_eind' => 223,
            'rit_start' => 'Aug 25, 2020 8:15:00 AM',
            'rit_eind' => 'Aug 25, 2020 11:34:00 AM',
            'kenteken' => 'AB-123-CD',
            'beschrijving' => 'Naar school'
        ]);
    }
}
